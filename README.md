> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368 - Advanced Web Applications Development

## Stephen Phillips

### Assignment # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install git
    - Create bitbucket account
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Install MySQL
	- Follow Tutorial on WebServlets
	- Create WebServlets
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Install MySQL WorkBench
	- Create Database
	- Forward Engineer
4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Finish Homepage
	- Link to previous assignments
	- Finish P1 tab
5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Complete and compile Customer.java
	- Complete and compile CustomerListServlet.java
	- Perform server and client-side validation
6. [A5 README.md](a5/README.md "My A5 README.md file")
	- Complete and compile files in data folder
	- Complete context.xml
	- Perform server and validation
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Complete and compile files in data folder
	- Complete context.xml
	- Test webpage and functionality