> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Stephen Phillips

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running Http://localhost:9999
* git commands with short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - This command creates an empty git repository or reinitializes an existing one.
2. git status - This command shows the repository status.
3. git add - This command adds a change in the working repository to the staging area.
4. git commit - This command records changes to the working repository.
5. git push - This command pushes the changes to the local repository to the remote repository.
6. git pull - This command will pull files from a remote repository into the local repository.
7. git clone - This command will clone a repository into a new directory.

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![AMPPS Installation Screenshot](img/hello.png)

*Screenshot of AMPPS running http://localhost*:

![JDK Installation Screenshot](img/tomcat.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/stp15e/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/stp15e/myteamquotes/ "My Team Quotes Tutorial")
