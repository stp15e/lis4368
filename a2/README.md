> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Stephen Phillips

### Assignment 2 Requirements:

*Three Parts:*

1. Install MySQL
2. Create and run WebServlets
3. Chapter Questions (Chs 5 - 6)

#### README.md file should include the following items:

* Screenshots and links of various working Web Servlets

 
#### Assignment Screenshots:

*Screenshot of Directory Page*:

![Directory Listing](img/listing.png)

*Link to the First Hello Page*:

[Hello Page](http://localhost:9999/hello/)

*Link to Hello World Page*:

[Say Hello Page](http://localhost:9999/hello/sayhello)

*Link to the Second Hi Page*:

[Say Hi Page](http://localhost:9999/hello/sayhi)

*Link to Querybook Page*:

[Querybook Select Page](http://localhost:9999/hello/querybook.html)

*Screenshot of http://localhost:9999/hello/query?author=Mohammad+Ali*:

![Querybook Results Page](img/results.png)
