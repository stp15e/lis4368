> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Stephen Phillips

### Assignment 3 Requirements:

*Three Parts:*

1. Install MySQL WorkBench
2. Create Database Tables and Insert Data
3. Forward Engineer Database

#### README.md file should include the following items:

* Screenshots and links of various working Web Servlets

 
#### Assignment Screenshots:

*Screenshot of Directory Page*:

![A3 ERD Screenshot](img/erd.png)

[ERD MWB File](https://bitbucket.org/stp15e/lis4368/src/10a5dab2bf084156d7003fd9935df28fbc622d21/a3/stp15e.mwb?at=master&fileviewer=file-view-default)

[ERD SQL File](https://bitbucket.org/stp15e/lis4368/src/10a5dab2bf084156d7003fd9935df28fbc622d21/a3/stp15e.sql?at=master&fileviewer=file-view-default)


