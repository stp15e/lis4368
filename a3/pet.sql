/*
-- Query: 
-- Date: 2017-04-12 15:34
*/
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0010,1000,0001,'German Shepherd','m',600,1,'Brown','01/21/2017','y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0020,1100,0002,'Calico','f',100,4,'Calico',NULL,'y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0030,1200,0003,'Husky','m',800,1,'White','01/24/2017','y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0040,1300,0004,'Malamute','f',800,2,'White','02/21/2017','y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0050,1400,0005,'Labrador','f',400,2,'Black',NULL,'y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0060,1500,0006,'Collie','f',400,1,'Brown','01/01/2017','y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0070,1600,0007,'Border Collie','m',500,1,'Black','12/23/2016','y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0080,1700,0008,'Siamese','m',150,2,'Grey','12/01/2016','y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0090,1800,0009,'American Shorthair','f',100,3,'Grey',NULL,'y','y',NULL);
INSERT INTO `pet` (`pet_id`,`pst_id`,`cus_id`,`pet_type`,`pet_sex`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_fixed`,`pet_notes`) VALUES (0099,1900,00010,'Maine Coon','f',100,1,'Brown',NULL,'y','y',NULL);
