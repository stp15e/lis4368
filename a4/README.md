> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Stephen Phillips

### Assignment 4 Requirements:

*Three Parts:*

	1. Complete and compile Customer.java
	2. Complete and compile CustomerListServlet.java
	3. Perform server and client-side validation

#### README.md file should include the following items:

* Screenshots and links of various working Web Servlets

 
#### Assignment Screenshots:

*Screenshot of Directory Page*:

![A4 Form Screenshot](img/form.png)

![A4 Server Side Validation Screenshot](img/form2.png)



