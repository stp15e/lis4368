> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Stephen Phillips

### Project 1 Requirements:

*Three Parts:*

1. Finish Carousel Homepage
2. Link to previous assignments
3. Finish Project 1 tab

#### README.md file should include the following items:

* Screenshots and links of various working Web Servlets

 
#### Assignment Screenshots:

*Screenshot of Directory Page*:

![P1 Screenshot](img/p1.png)

![P1 Homepage](img/home.png)

![P1 Pass Screenshot](img/pass.png)


