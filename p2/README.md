> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Stephen Phillips

### Assignment 5 Requirements:

*Three Parts:*

	1. Complete and compile files in data folder
	2. Create Modify.jsp
	3. Create CustomerServlet.java


#### README.md file should include the following items:

* Screenshots and links of various working Web Servlets

 
#### Assignment Screenshots:

*Screenshot of Directory Page*:

![P2 Form Screenshot](img/form.png)

![P2 Passed Validation](img/form1.png)

![P2 Display Data](img/form3.png)

![P2 Modify Form](img/form4.png)

![P2 Passed Validation](img/form1.png)

![P2 Delete Warning](img/form5.png)

![P2 Associated Database Changes](img/sql.png)

![P2 Associated Database Update](img/update.png)







